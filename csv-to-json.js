const csv = require('csv-parser');
const fs = require('fs');

const file = process.argv[2];
let arr = [];

fs.createReadStream(file)
    .pipe(csv())
    .on('data', (row) => {
        arr.push(row);
    })
    .on('end', () => {
        fs.writeFileSync('strucuture.json', JSON.stringify(arr, null, 4));
    })